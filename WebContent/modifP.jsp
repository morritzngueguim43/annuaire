<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Update data of a Member</title>
		<link rel="stylesheet" href="inc/creation.css">
	</head>
	<body>
		<form action="update" method="GET">
			<h3>Update Member!</h3>
			<div class="container">
				<div>
					<label for="matriculeA">Enter Matricul </label><br>
					<input type="text" id="matriculeA" name="matriculeA" placeholder="ca-udm-22sci-9025" required>
				</div>
				<div>
					<label for="matriculeN">Enter new Matricul </label><br>
					<input type="text" id="matriculeN" name="matriculeN" placeholder="ca-udm-20sci-2505" required>
				</div>
				<div>
					<label for="nom">Enter Name</label><br>
					<input type="text" id="nom" name="nom" placeholder="Dupont">
				</div>
				<div>
					<label for="prenom">Enter Surname</label><br>
					<input type="text" id="prenom" name="prenom" placeholder="Alexia">
				</div>
				<div>
					<label for="telephone">Enter Phone</label><br>
					<input type="text" id="telephone" name="telephone" placeholder="+33 257-536-0055">
				</div>
			</div>
			<button type="submit">Submit</button>
			<h6>Made by Genuis @JoelY_K</h6>
		</form>
	</body>
</html>