<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; utf-8">
		<title>Welcome dear customer</title>
		<link type="text/css" rel="stylesheet" href="inc/menu.css" />
	</head>
	<body>
		<form action="menu" method="GET">
			<div class="container">
				<div class="notif">${message }</div>
				<h3 class="intitul�">Choose one offer </h3>
				<div>
					<input type="radio" id="record" name="option" value="record">
					<label for="record">Record a Member</label>
				</div>
				<div>
					<input type="radio" id="infos" name="option" value="infos">
					<label for="infos">Get Member informations</label>
				</div>
				<div>
					<input type="radio" id="infosAll" name="option" value="infosAll">
					<label for="infosAll">Get Members informations</label>
				</div>
				<div>
					<input type="radio" id="update" name="option" value="update">
					<label for="update">Update a Member</label>
				</div>
			</div>
			<button type="submit">Submit</button>
			<h6>Made by Genuis @JoelY_K</h6>
		</form>
	</body>
</html>