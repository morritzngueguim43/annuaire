<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Recherche d'une personne</title>
		<link rel="stylesheet" href="inc/creation.css" >
	</head>
	<body>
		<form action="recherche" method="GET">
			<h3>Veuillez saisir le matricule de la personne dont vous voulez les informations</h3>
			<div>
				<input type="text" name="matricule" required>
			</div>
			<button type="submit">Envoyer</button>
			<h6>Made by Genuis @JoelY_K</h6>
		</form>
	</body>
</html>