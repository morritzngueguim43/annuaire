<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Member informations</title>
<link rel="stylesheet" href="inc/infoP.css">
</head>
<body>
	<div class="notif">
		${message}
	</div>
	<div class="personne">
		<label>Matricul : ${personne.matricule}</label><br>
		<label>Nom : ${personne.nom}</label><br>
		<label>Prenom : ${personne.prenom}</label><br>
		<label>Phone : ${personne.telephone}</label><br>
	</div>
	<a href="menu.jsp"><label>Home</label></a><br>
	<h6>Made by Genuis @JoelY_K</h6>
</body>
</html>