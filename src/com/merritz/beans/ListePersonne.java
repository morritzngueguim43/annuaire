package com.merritz.beans;

import java.util.HashMap;

public class ListePersonne {
	private HashMap<String, Personne> listeP;

	/**
	 * @return the listeP
	 */
	public HashMap<String, Personne> getListeP() {
		return listeP;
	}
	
	public void setListeP(HashMap<String, Personne> listeP) {
		this.listeP = listeP;
	}
	
	/**
	 * methode d'ajout d'une personne a la liste
	 */
	public void addPersonneToListePersonne(Personne p){
		HashMap<String, Personne> m = this.getListeP();
		m.put(p.getMatricule(), p);
		this.setListeP(m);;
	}
	
	/**
	 * fonction de recherche d'une personne
	 */
	public Personne searchPersonneInListePersonne(String matricule){
		return getListeP().get(matricule);
	}
	
	/**
	 *fonction de modification d'une personne 
	 */
	public int updateByKey(String matriculeA,String matriculeN,String nom,String prenom,String telephone){
		Personne p = searchPersonneInListePersonne(matriculeA);
		if(p == null){
			return 0;
		}else{
			p.setMatricule(matriculeN);
			p.setNom(nom);
			p.setPrenom(prenom);
			p.setTelephone(telephone);
			getListeP().remove(matriculeA);
			getListeP().put(matriculeN, p);
			return 1;
		}
	}
}
