package com.merritz.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.ListePersonne;
import com.merritz.beans.Personne;

@SuppressWarnings("serial")
public class RechercherPersonne extends HttpServlet {
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException, ServletException{
		String matricule = request.getParameter("matricule");
		HttpSession session = request.getSession();
		ListePersonne listeP = (ListePersonne)session.getAttribute("listeP");
		String message;
		
		if(listeP != null){
			message = "Information relative au matricule "+matricule;
			Personne p = listeP.searchPersonneInListePersonne(matricule);
			request.setAttribute("personne", p);
		}else{
			message = "la personne dont le matricule est "+matricule+" n'a pas �t� trouv�e !";
		}
		request.setAttribute("message", message);
		session.setAttribute("listeP", listeP);
		request.setAttribute("sesion", session);
		
		request.getServletContext().getRequestDispatcher("/infoP.jsp").forward(request, response);
	}
}
