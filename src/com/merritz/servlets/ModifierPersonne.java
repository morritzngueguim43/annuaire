package com.merritz.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.ListePersonne;

@SuppressWarnings("serial")
public class ModifierPersonne extends HttpServlet {
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		String matriculeA = request.getParameter("matriculeA");
		String matriculeN = request.getParameter("matriculeN");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("telephone");
		HttpSession session = request.getSession();
		ListePersonne listeP = (ListePersonne)session.getAttribute("listeP");
		String message;
		
		if(listeP != null && listeP.updateByKey(matriculeA, matriculeN, nom, prenom, telephone) != 0){
			message = "Mise � jour effectuer avec succ�s !<br>Consulter la liste pour verifier !";
		}else{
			message = "erreur lors de la mise � jour !<br>Veuillez re�ssayer ulterieurement !";
		}
		request.setAttribute("message", message);
		session.setAttribute("listeP", listeP);
		request.setAttribute("session", session);
		
		request.getServletContext().getRequestDispatcher("/menu.jsp").forward(request, response);
	}
}
