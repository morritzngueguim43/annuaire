package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.ListePersonne;
import com.merritz.beans.Personne;

@SuppressWarnings("serial")
public class CreationPersonne extends HttpServlet {
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		//recuperation des donn�es envoy�s
		String matricule = request.getParameter("matricule");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("telephone");
		
		//initialisation du bean Personne
		Personne p = new Personne();
		p.setMatricule(matricule);
		p.setNom(nom);
		p.setPrenom(prenom);
		p.setTelephone(telephone);
		
		//gestion de la liste des personne
		ListePersonne listeP = new ListePersonne();
		HashMap<String, Personne> m = new HashMap<String, Personne>();
		m.put(p.getMatricule(), p);
		listeP.setListeP(m);
		
		//recuperation de la session
		HttpSession session = request.getSession();
		if(session.getAttribute("listeP") == null){
			session.setAttribute("listeP", listeP);
		}else{
			((ListePersonne)session.getAttribute("listeP")).addPersonneToListePersonne(p);
		}
		request.setAttribute("session", session);
		request.setAttribute("message", "Insertion reussie !");
		
		System.out.println(((ListePersonne)session.getAttribute("listeP")).getListeP());
		
		request.getServletContext().getRequestDispatcher("/menu.jsp?set=ok").forward(request, response);
	}
}
