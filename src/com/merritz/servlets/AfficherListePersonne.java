package com.merritz.servlets;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.ListePersonne;
import com.merritz.beans.Personne;


@SuppressWarnings("serial")
public class AfficherListePersonne extends HttpServlet {
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		
		String param = request.getParameter("option");
		String link;
		String message = "<tr>"
				+ "	<td colspan=\"4\">La liste est actuellement vide !<br>Veuillez enregistrer des membres !</td>"
				+ "</tr>";
		HttpSession session = request.getSession();
		ListePersonne listeP = (ListePersonne)session.getAttribute("listeP");
		
		
		if(param == null){
			link = "/menu.jsp";
		}else if(param.equals("record")){
			link = "/creation.jsp";
		}else if(param.equals("infos")){
			link = "/demandeP.jsp";
		}else if(param.equals("infosAll")){
			if(listeP != null){
				message = "";
				Collection <Personne> col = listeP.getListeP().values();
				Iterator<Personne> p = col.iterator();
				while(p.hasNext()){
					Personne p1 = p.next();
					message += "<div name=\"personne\">"
							+ "		<label>Matricul : "+p1.getMatricule()+"</label><br>"
							+ "		<label>Nom : "+p1.getNom()+"</label><br>"
							+ "		<label>Prenom : "+p1.getPrenom()+"</label><br>"
							+ "		<label>Phone : "+p1.getTelephone()+"</label><br>"
							+ "</div>";
				}
			}
			request.setAttribute("message", message);
			link = "/infoListeP.jsp";
		}else if(param.equals("update")){
			link = "/modifP.jsp";
		}else{
			link = "/menu.jsp";
		}
		session.setAttribute("listeP", listeP);
		request.setAttribute("session", session);
		
		request.getServletContext().getRequestDispatcher(link).forward(request, response);
	}
}
